package de.kernblick.opcuasimulator.server;

import java.util.concurrent.ExecutionException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import de.kernblick.opcuasimulator.client.OpcUaSimulatorClient;

public class OpcUaServerTest {

    OpcUaSimulatorServer server;

    @BeforeSuite
    public void BeforeSuite() throws Exception {
        server = new OpcUaSimulatorServer("0.0.0.0", "0.0.0.0", 4840, null, "OPCUAServer.NodeSet2.xml", "anonymous", "", false);
        server.startup().get();
    }

    @Test
    public void testServerStart() throws Exception {
        OpcUaSimulatorClient client = new OpcUaSimulatorClient();
        
        // write value and check if value is set after read
        client.writeNode("ns=1;s=TestMachine_MachineInformation_MachineStatus_MachineNumber", null);
        org.testng.Assert.assertEquals(null, client.readNode("ns=1;s=TestMachine_MachineInformation_MachineStatus_MachineNumber"));


        // write value and check if value is set after read
        client.writeNode("ns=1;s=TestMachine_MachineInformation_MachineStatus_MachineNumber", "0815");
        org.testng.Assert.assertEquals("0815", client.readNode("ns=1;s=TestMachine_MachineInformation_MachineStatus_MachineNumber"));

        // call method and expect result "true"
        org.testng.Assert.assertTrue((boolean) client.callMethod("ns=2;s=ControlServer", "ns=2;s=triggerEvent", "i=2041").get());

    }

    @AfterSuite
    public void AfterSuite() throws InterruptedException {
        if(server != null) {
            server.shutdown();
        }
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException, Exception {
        new OpcUaSimulatorServer("0.0.0.0", "0.0.0.0", 4840, null, "OPCUAServer.NodeSet2.xml", "anonymous", "", false)
        .startup().get();

        Thread.sleep(150000);
    }
    
}

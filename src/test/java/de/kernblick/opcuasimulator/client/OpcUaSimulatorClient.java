/*
 * Copyright (c) 2019 the Eclipse Milo Authors
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package de.kernblick.opcuasimulator.client;

import static com.google.common.collect.Lists.newArrayList;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import com.google.common.collect.ImmutableList;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.identity.AnonymousProvider;
import org.eclipse.milo.opcua.sdk.client.api.identity.IdentityProvider;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaSubscription;
import org.eclipse.milo.opcua.sdk.client.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.client.security.DefaultClientCertificateValidator;
import org.eclipse.milo.opcua.stack.core.AttributeId;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.security.DefaultTrustListManager;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.MonitoringMode;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.CallMethodRequest;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoringParameters;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpcUaSimulatorClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private DefaultTrustListManager trustListManager;
    private OpcUaClient client;

    private String endpointUrl = "opc.tcp://localhost:4840";

    public OpcUaSimulatorClient() throws Exception {
        this(null);
    }

    public OpcUaSimulatorClient(String endpointUrl) throws Exception {
        if(endpointUrl != null) this.endpointUrl = endpointUrl;

        client = createClient();
        client.connect().get();
    }

    public void disconnect() {
        if (client != null) {
            client.disconnect();
        }
    }

    public OpcUaClient createClient() throws Exception {
        Path securityTempDir = Paths.get(System.getProperty("java.io.tmpdir"), "client", "security");
        Files.createDirectories(securityTempDir);
        if (!Files.exists(securityTempDir)) {
            throw new Exception("unable to create security dir: " + securityTempDir);
        }

        File pkiDir = securityTempDir.resolve("pki").toFile();

        LoggerFactory.getLogger(getClass())
            .info("security dir: {}", securityTempDir.toAbsolutePath());
        LoggerFactory.getLogger(getClass())
            .info("security pki dir: {}", pkiDir.getAbsolutePath());

        KeyStoreLoader loader = new KeyStoreLoader().load(securityTempDir);

        trustListManager = new DefaultTrustListManager(pkiDir);

        DefaultClientCertificateValidator certificateValidator =
            new DefaultClientCertificateValidator(trustListManager);

        return OpcUaClient.create(
            this.getEndpointUrl(),
            endpoints ->
                endpoints.stream()
                    .filter(this.endpointFilter())
                    .findFirst(),
            configBuilder ->
                configBuilder
                    .setApplicationName(LocalizedText.english("KERNBLICK OPC UA Simulator Client"))
                    .setApplicationUri("urn:kernblick:opcuasimulator:client")
                    .setKeyPair(loader.getClientKeyPair())
                    .setCertificate(loader.getClientCertificate())
                    .setCertificateChain(loader.getClientCertificateChain())
                    .setCertificateValidator(certificateValidator)
                    .setIdentityProvider(this.getIdentityProvider())
                    .setRequestTimeout(uint(5000))
                    .build()
        );
    }


    public void writeNode(String nodeId, Object value) throws InterruptedException, ExecutionException {
        writeNode(NodeId.parse(nodeId), value);
    }

    public void writeNode(NodeId nodeId, Object value) throws InterruptedException, ExecutionException {
        List<NodeId> nodeIds = ImmutableList.of(nodeId);

        Variant v = new Variant(value);

        // don't write status or timestamps
        DataValue dv = new DataValue(v, null, null);

        // write asynchronously....
        CompletableFuture<List<StatusCode>> future =
            client.writeValues(nodeIds, ImmutableList.of(dv));

        // ...but block for the results so we write in order
        List<StatusCode> statusCodes = future.get();
        StatusCode status = statusCodes.get(0);

        if (status.isGood()) {
            logger.info("Wrote '{}' to nodeId={}", v, nodeIds.get(0));
        } else {
            logger.warn(status.toString());
        }

    }

    public String readNode(String nodeId) throws InterruptedException, ExecutionException, UaException {
        return readNode(NodeId.parse(nodeId));
    }

    public String readNode(NodeId nodeId) throws InterruptedException, ExecutionException, UaException {

        // synchronous read request via VariableNode
        UaVariableNode node = client.getAddressSpace().getVariableNode(nodeId);
        DataValue value = node.readValue();

        logger.info("Read '{}' from nodeId={}", value, nodeId);

        return value.getValue().getValue() == null ? null : value.getValue().getValue().toString();
    }

    public CompletableFuture<Object> callMethod(String objectId, String methodId) {
        return callMethod(NodeId.parse(objectId), NodeId.parse(methodId), null);
    }

    public CompletableFuture<Object> callMethod(String objectId, String methodId, String argument1, String argument2) {
        // if only one argument is provided, we add the optional payload as second empty argument
        return callMethod(objectId, methodId, new Variant[]{ new Variant(argument1), new Variant(argument2) });
    }

    public CompletableFuture<Object> callMethod(String objectId, String methodId, String argument) {
        // if only one argument is provided, we add the optional payload as second empty argument
        return callMethod(objectId, methodId, new Variant[]{ new Variant(argument), new Variant("") });
    }

    public CompletableFuture<Object> callMethod(String objectId, String methodId, Variant[] arguments) {
        return callMethod(NodeId.parse(objectId), NodeId.parse(methodId), arguments);
    }

    public CompletableFuture<Object> callMethod(NodeId objectId, NodeId methodId, Variant[] arguments) {

        CallMethodRequest request = new CallMethodRequest(
            objectId,
            methodId,
            arguments == null ? new Variant[]{} : arguments
        );

        return client.call(request).thenCompose(result -> {
            StatusCode statusCode = result.getStatusCode();

            if (statusCode.isGood()) {

                Object returnValue = true;

                Variant[] outputs = result.getOutputArguments();
                if(outputs.length > 0) {
                    returnValue = outputs[0].getValue().toString();
                }

                logger.info("Called method '{}' with result={}", methodId, returnValue);

                return CompletableFuture.completedFuture(returnValue);

            } else {
                CompletableFuture<Object> f = new CompletableFuture<>();
                f.completeExceptionally(new UaException(statusCode));

                logger.warn("Error while calling method '{}': {}", methodId, result);

                return f;
            }
        });
    }

    public void subscribe(NodeId nodeId) throws Exception {

        // create a subscription @ 1000ms
        UaSubscription subscription = client.getSubscriptionManager().createSubscription(10.0).get();

        // subscribe to the Value attribute of the server's CurrentTime node
        ReadValueId readValueId = new ReadValueId(
            // Identifiers.Server_ServerStatus_CurrentTime,
            nodeId,
            AttributeId.Value.uid(), null, QualifiedName.NULL_VALUE
        );

        // IMPORTANT: client handle must be unique per item within the context of a subscription.
        // You are not required to use the UaSubscription's client handle sequence; it is provided as a convenience.
        // Your application is free to assign client handles by whatever means necessary.
        UInteger clientHandle = subscription.nextClientHandle();

        MonitoringParameters parameters = new MonitoringParameters(
            clientHandle,
            10.0,     // sampling interval
            null,       // filter, null means use default
            uint(10),   // queue size
            true        // discard oldest
        );

        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(
            readValueId,
            MonitoringMode.Reporting,
            parameters
        );

        // when creating items in MonitoringMode.Reporting this callback is where each item needs to have its
        // value/event consumer hooked up. The alternative is to create the item in sampling mode, hook up the
        // consumer after the creation call completes, and then change the mode for all items to reporting.
        UaSubscription.ItemCreationCallback onItemCreated =
            (item, id) -> item.setValueConsumer(this::onSubscriptionValue);

        List<UaMonitoredItem> items = subscription.createMonitoredItems(
            TimestampsToReturn.Both,
            newArrayList(request),
            onItemCreated
        ).get();

        for (UaMonitoredItem item : items) {
            if (item.getStatusCode().isGood()) {
                logger.info("item created for nodeId={}", item.getReadValueId().getNodeId());
            } else {
                logger.warn(
                    "failed to create item for nodeId={} (status={})",
                    item.getReadValueId().getNodeId(), item.getStatusCode());
            }
        }
    }

    private void onSubscriptionValue(UaMonitoredItem item, DataValue value) {
        logger.info(
            "subscription value received: item={}, value={}",
            item.getReadValueId().getNodeId(), value.getValue());
    }

    String getEndpointUrl() {
        return endpointUrl;
    }

    Predicate<EndpointDescription> endpointFilter() {
        return e -> getSecurityPolicy().getUri().equals(e.getSecurityPolicyUri());
    }

    SecurityPolicy getSecurityPolicy() {
        return SecurityPolicy.None;
    }

    IdentityProvider getIdentityProvider() {
        return new AnonymousProvider();
    }

}


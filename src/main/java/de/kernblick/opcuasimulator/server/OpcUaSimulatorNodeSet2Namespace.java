/*
 * Copyright (c) 2022 KERNBLICK GmbH
 */

package de.kernblick.opcuasimulator.server;

import static de.kernblick.opcuasimulator.nodeset.util.NodeSetUtils.parseNodeSet;

import java.util.List;

import javax.xml.bind.JAXBException;

import org.eclipse.milo.opcua.sdk.server.Lifecycle;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.DataItem;
import org.eclipse.milo.opcua.sdk.server.api.ManagedNamespaceWithLifecycle;
import org.eclipse.milo.opcua.sdk.server.api.MonitoredItem;
import org.eclipse.milo.opcua.sdk.server.dtd.DataTypeDictionaryManager;
import org.eclipse.milo.opcua.sdk.server.util.SubscriptionModel;
import org.opcfoundation.ua.generated.UANodeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kernblick.opcuasimulator.nodeset.UaNodeSet;

public class OpcUaSimulatorNodeSet2Namespace extends ManagedNamespaceWithLifecycle {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    // private final Random random = new Random();

    private final DataTypeDictionaryManager dictionaryManager;

    private final SubscriptionModel subscriptionModel;

    private UANodeSet nodeset2;

    OpcUaSimulatorNodeSet2Namespace(OpcUaServer server, String namespaceUri, UANodeSet nodeset2) {
        super(server, namespaceUri);

        if (nodeset2 != null)
            this.nodeset2 = nodeset2;

        subscriptionModel = new SubscriptionModel(server, this);
        dictionaryManager = new DataTypeDictionaryManager(getNodeContext(), namespaceUri);

        getLifecycleManager().addLifecycle(dictionaryManager);
        getLifecycleManager().addLifecycle(subscriptionModel);

        getLifecycleManager().addStartupTask(this::createAndAddNodes);

        getLifecycleManager().addLifecycle(new Lifecycle() {
            @Override
            public void startup() {
            }

            @Override
            public void shutdown() {
            }
        });
    }

    private void createAndAddNodes() {

        logger.info("Loading NodeSet2 for " + getNamespaceUri());
        addNodes(nodeset2);

    }

    /**
     * Adds the given NodeSet2 to the OPC UA
     * {@link org.eclipse.milo.opcua.sdk.server.api.AddressSpace}
     *
     * @param nodeSetXml
     */
    private void addNodes(UANodeSet nodeSetXml) {

        try {
            UaNodeSet nodeSet = UaNodeSet.parse(nodeSetXml);
            parseNodeSet(nodeSet, getNodeContext(), getNodeManager());
        } catch (JAXBException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void onDataItemsCreated(List<DataItem> dataItems) {
        subscriptionModel.onDataItemsCreated(dataItems);
    }

    @Override
    public void onDataItemsModified(List<DataItem> dataItems) {
        subscriptionModel.onDataItemsModified(dataItems);
    }

    @Override
    public void onDataItemsDeleted(List<DataItem> dataItems) {
        subscriptionModel.onDataItemsDeleted(dataItems);
    }

    @Override
    public void onMonitoringModeChanged(List<MonitoredItem> monitoredItems) {
        subscriptionModel.onMonitoringModeChanged(monitoredItems);
    }

}

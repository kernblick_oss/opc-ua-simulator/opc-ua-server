/*
 * Copyright (c) 2019 the Eclipse Milo Authors
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package de.kernblick.opcuasimulator.server.methods;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ushort;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.milo.opcua.sdk.core.ValueRanks;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.methods.AbstractMethodInvocationHandler;
import org.eclipse.milo.opcua.sdk.server.api.methods.InvalidArgumentException;
import org.eclipse.milo.opcua.sdk.server.model.nodes.objects.BaseEventTypeNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.eclipse.milo.opcua.stack.core.types.builtin.DiagnosticInfo;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.StatusCode;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.structured.Argument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kernblick.citrus.opcua.ArgumentModel;

public class GenerateEventMethod extends AbstractMethodInvocationHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public static final Argument EVENT_TYPE_ID = new Argument(
        "EventTypeId",
        Identifiers.String,
        ValueRanks.Any,
        null,
        new LocalizedText("NodeId (string) of the TypeDefinition of the event to generate.")
    );

    public static final Argument PAYLOAD = new Argument(
        "Payload",
        Identifiers.String,
        ValueRanks.Scalar,
        null,
        new LocalizedText("XML payload to be send by the event.")
    );

    private final OpcUaServer server;

    public GenerateEventMethod(UaMethodNode methodNode) {
        super(methodNode);

        this.server = methodNode.getNodeContext().getServer();
    }

    @Override
    public Argument[] getInputArguments() {
        return new Argument[]{EVENT_TYPE_ID, PAYLOAD};
    }

    @Override
    public Argument[] getOutputArguments() {
        return new Argument[0];
    }

    @Override
    protected Variant[] invoke(InvocationContext invocationContext, Variant[] inputValues) throws UaException {
        String eventTypeIdStr = (String) inputValues[0].getValue();
        String payload = (String) inputValues[1].getValue();

        NodeId eventTypeId =  null;
        UaNode eventToThrow = null;

        try {

            // get event to throw
            eventTypeId = NodeId.parse(eventTypeIdStr);
            eventToThrow = server.getAddressSpaceManager().getManagedNode(eventTypeId).orElseThrow();


        } catch (Exception ex) {
            String message = "Could not parse EventTypeId " + eventTypeIdStr + ". Are you providing a full NodeId descriptor like ns=1;i=1234 or i=2041 ?";
            LOGGER.error(message);
            throw new InvalidArgumentException(new StatusCode[] { StatusCode.BAD }, new DiagnosticInfo[] { new DiagnosticInfo(-1, -1, -1, -1, message, StatusCode.BAD, null) });
        }

        // Create method data as event payload
        GenerateEventModel model = new GenerateEventModel();
        model.setBrowseName(eventToThrow.getBrowseName().toParseableString());
        model.setDisplayName(eventToThrow.getDisplayName().getText());
        model.setNodeId(eventToThrow.getNodeId().toParseableString());

        // add payload if any
        if(payload != null && !payload.isEmpty()) {
            if(isXml(payload)) {

                try {
                    JAXBContext context = JAXBContext.newInstance( ArgumentModel.class );
                    Unmarshaller unmarshaller = context.createUnmarshaller();
                    ArgumentModel payloadObject = (ArgumentModel) unmarshaller.unmarshal(new StringReader(payload));
                    model.addArgument(payloadObject);
                } catch (JAXBException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                    throw new InvalidArgumentException(
                        new StatusCode[] { StatusCode.GOOD, StatusCode.BAD },
                        new DiagnosticInfo[] { DiagnosticInfo.NULL_VALUE, new DiagnosticInfo(-1, -1, -1, -1, ex.getLocalizedMessage(), StatusCode.BAD, null) });
                }

            } else {

                LOGGER.debug("The payload provided to this method call is not an <Argument>");
                ArgumentModel arg = new ArgumentModel();
                arg.setName("payload");
                arg.setValue(payload);
                arg.setType("string");
                model.addArgument(arg);

            }
        }

        // Create XML for data
        StringWriter sw = new StringWriter();
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(GenerateEventModel.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.marshal(model, sw);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Sending the following event message:");
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(model, System.out);
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        String message = sw.toString();

        // ******
        // do: now creating event and sending to eventbus

        BaseEventTypeNode eventNode = server.getEventFactory().createEvent(
            new NodeId(1, UUID.randomUUID()),
            Identifiers.BaseEventType
        );

        eventNode.setBrowseName(new QualifiedName(1, "event"));
        eventNode.setDisplayName(LocalizedText.english("event"));
        eventNode.setEventId(ByteString.of(new byte[]{0, 1, 2, 3}));
        eventNode.setEventType(eventTypeId);
        eventNode.setSourceNode(eventToThrow.getNodeId());
        eventNode.setSourceName(eventToThrow.getDisplayName().getText());
        eventNode.setTime(DateTime.now());
        eventNode.setReceiveTime(DateTime.NULL_VALUE);
        eventNode.setMessage(LocalizedText.english(message));
        eventNode.setSeverity(ushort(2));

        server.getEventBus().post(eventNode);

        eventNode.delete();

        // done
        // ******

        return new Variant[0];
    }

    private boolean isXml(Object object) {
        if (!(object instanceof String)) return false;

        String document = (String) object;
        if (!document.contains("<")) return false;

        try {
            DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(document);
            return true;
        } catch(Exception e) {
            return false;
        }
    }

}

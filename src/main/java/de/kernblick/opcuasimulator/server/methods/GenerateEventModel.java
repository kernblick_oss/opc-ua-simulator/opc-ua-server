package de.kernblick.opcuasimulator.server.methods;


import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.kernblick.citrus.opcua.ArgumentModel;


@XmlRootElement(name="Event")
@XmlAccessorType (XmlAccessType.FIELD)
public class GenerateEventModel {

    @XmlElement(name="BrowseName")
    String browseName;

    @XmlElement(name="NodeId")
    String nodeId;

    @XmlElement(name="DisplayName")
    String displayName;

    // @XmlElementWrapper(name = "Arguments")
    @XmlElement(name = "Argument")
    public List<ArgumentModel> arguments;

    @XmlAnyElement(lax = true)
    protected List<Object> any;

    public String getBrowseName() {
        return this.browseName;
    }

    public void setBrowseName(String browseName) {
        this.browseName = browseName;
    }

    public String getNodeId() {
        return this.nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<ArgumentModel> getArguments() {
        return this.arguments;
    }

    public void setArguments(List<ArgumentModel> arguments) {
        this.arguments = arguments;
    }

    public void addArgument(ArgumentModel argument) {
        if (this.arguments == null) this.arguments = new LinkedList<>();
        this.arguments.add(argument);
    }

    public List<Object> getAny() {
        return this.any;
    }

    public void setAny(List<Object> any) {
        this.any = any;
    }


    /**
     * Payload object may contain anything
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name="Payload")
    @XmlType(name = "Payload", propOrder = {
        "any"
    })
    static public class Payload {

        @XmlAnyElement(lax = true)
        protected List<Object> any;

    }

}

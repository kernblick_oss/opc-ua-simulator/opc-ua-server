/*
 * Copyright (c) 2022 KERNBLICK GmbH
 */

package de.kernblick.opcuasimulator.server;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ubyte;

import java.util.List;

import org.eclipse.milo.opcua.sdk.core.AccessLevel;
import org.eclipse.milo.opcua.sdk.core.Reference;
import org.eclipse.milo.opcua.sdk.server.Lifecycle;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.DataItem;
import org.eclipse.milo.opcua.sdk.server.api.ManagedNamespaceWithLifecycle;
import org.eclipse.milo.opcua.sdk.server.api.MonitoredItem;
import org.eclipse.milo.opcua.sdk.server.dtd.DataTypeDictionaryManager;
import org.eclipse.milo.opcua.sdk.server.nodes.UaFolderNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaVariableNode;
import org.eclipse.milo.opcua.sdk.server.util.SubscriptionModel;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kernblick.opcuasimulator.server.methods.GenerateEventMethod;

public class OpcUaSimulatorControlServerNamespace extends ManagedNamespaceWithLifecycle {

    public static final String NAMESPACE_URI = "http://kernblick.de/UA/OpcUaSimulator/ControlServer/";

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    // private final Random random = new Random();

    private final DataTypeDictionaryManager dictionaryManager;

    private final SubscriptionModel subscriptionModel;

    /** namespace name */
    private String name = "ControlServer";

    private String pomVersion;

    OpcUaSimulatorControlServerNamespace(OpcUaServer server, String version) {
        super(server, NAMESPACE_URI);

        pomVersion = version;

        subscriptionModel = new SubscriptionModel(server, this);
        dictionaryManager = new DataTypeDictionaryManager(getNodeContext(), NAMESPACE_URI);

        getLifecycleManager().addLifecycle(dictionaryManager);
        getLifecycleManager().addLifecycle(subscriptionModel);

        getLifecycleManager().addStartupTask(this::createAndAddNodes);

        getLifecycleManager().addLifecycle(new Lifecycle() {
            @Override
            public void startup() {
            }

            @Override
            public void shutdown() {
            }
        });
    }

    public OpcUaSimulatorControlServerNamespace name(String name) {
        this.name = name;
        return this;
    }

    private void createAndAddNodes() {

        LOGGER.info("Adding ControlServer nodes.");

        // Create a namespace folder and add it to the node manager
        NodeId folderNodeId = newNodeId(name);

        UaFolderNode folderNode = new UaFolderNode(
            getNodeContext(),
            folderNodeId,
            newQualifiedName(name),
            LocalizedText.english(name)
        );

        folderNode.setEventNotifier(ubyte(1));

        getNodeManager().addNode(folderNode);

        // Make sure our new folder shows up under the server's Objects folder.
        folderNode.addReference(new Reference(
            folderNode.getNodeId(),
            Identifiers.Organizes,
            Identifiers.ObjectsFolder.expanded(),
            false
        ));

        addTriggerEventMethod(folderNode);

        addVersionVariable(folderNode);
    }

    private void addVersionVariable(UaFolderNode folderNode) {
        UaVariableNode node = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
            .setNodeId(newNodeId("version"))
            .setAccessLevel(AccessLevel.READ_WRITE)
            .setUserAccessLevel(AccessLevel.READ_WRITE)
            .setBrowseName(newQualifiedName("version"))
            .setDisplayName(LocalizedText.english("version"))
            .setDataType(Identifiers.String)
            .setTypeDefinition(Identifiers.BaseDataVariableType)
            .build();

        node.setValue(new DataValue(new Variant(pomVersion != null ? pomVersion : "1.0")));

        getNodeManager().addNode(node);
        folderNode.addOrganizes(node);
    }


    private void addTriggerEventMethod(UaFolderNode folderNode) {
        UaMethodNode methodNode = UaMethodNode.builder(getNodeContext())
            .setNodeId(newNodeId("triggerEvent"))
            .setBrowseName(newQualifiedName("triggerEvent"))
            .setDisplayName(new LocalizedText(null, "triggerEvent"))
            .setDescription(
                LocalizedText.english("Generate an Event with given EventId."))
            .build();

        GenerateEventMethod triggerEventMethod = new GenerateEventMethod(methodNode);
        methodNode.setInputArguments(triggerEventMethod.getInputArguments());
        methodNode.setOutputArguments(triggerEventMethod.getOutputArguments());
        methodNode.setInvocationHandler(triggerEventMethod);

        getNodeManager().addNode(methodNode);

        methodNode.addReference(new Reference(
            methodNode.getNodeId(),
            Identifiers.HasComponent,
            folderNode.getNodeId().expanded(),
            false
        ));
    }

    @Override
    public void onDataItemsCreated(List<DataItem> dataItems) {
        subscriptionModel.onDataItemsCreated(dataItems);
    }

    @Override
    public void onDataItemsModified(List<DataItem> dataItems) {
        subscriptionModel.onDataItemsModified(dataItems);
    }

    @Override
    public void onDataItemsDeleted(List<DataItem> dataItems) {
        subscriptionModel.onDataItemsDeleted(dataItems);
    }

    @Override
    public void onMonitoringModeChanged(List<MonitoredItem> monitoredItems) {
        subscriptionModel.onMonitoringModeChanged(monitoredItems);
    }

}

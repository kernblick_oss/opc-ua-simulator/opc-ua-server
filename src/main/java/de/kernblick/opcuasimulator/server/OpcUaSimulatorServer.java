/*
 * Copyright (c) 2022 KERNBLICK GmbH
 */

package de.kernblick.opcuasimulator.server;

import static com.google.common.collect.Lists.newArrayList;
import static org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig.USER_TOKEN_POLICY_ANONYMOUS;
import static org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig.USER_TOKEN_POLICY_USERNAME;
// import static org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig.USER_TOKEN_POLICY_X509;

// Security Imports:
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.ManagedNamespaceWithLifecycle;
import org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfig;
import org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfigBuilder;
import org.eclipse.milo.opcua.sdk.server.api.config.OpcUaServerConfigLimits;
import org.eclipse.milo.opcua.sdk.server.identity.UsernameIdentityValidator;
import org.eclipse.milo.opcua.sdk.server.util.HostnameUtil;
import org.eclipse.milo.opcua.stack.core.StatusCodes;
import org.eclipse.milo.opcua.stack.core.UaRuntimeException;
import org.eclipse.milo.opcua.stack.core.security.DefaultCertificateManager;
import org.eclipse.milo.opcua.stack.core.security.SecurityPolicy;
import org.eclipse.milo.opcua.stack.core.transport.TransportProfile;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UShort;
import org.eclipse.milo.opcua.stack.core.types.enumerated.MessageSecurityMode;
import org.eclipse.milo.opcua.stack.core.types.structured.BuildInfo;
import org.eclipse.milo.opcua.stack.core.util.CertificateUtil;
import org.eclipse.milo.opcua.stack.core.util.NonceUtil;
import org.eclipse.milo.opcua.stack.core.util.SelfSignedCertificateGenerator;
import org.eclipse.milo.opcua.stack.core.util.SelfSignedHttpsCertificateBuilder;
import org.eclipse.milo.opcua.stack.server.EndpointConfiguration;
import org.opcfoundation.ua.generated.ModelTable;
import org.opcfoundation.ua.generated.ModelTableEntry;
import org.opcfoundation.ua.generated.ObjectFactory;
import org.opcfoundation.ua.generated.UANodeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ushort;

import de.kernblick.opcuasimulator.server.security.ExternalIPUtil;
import de.kernblick.opcuasimulator.server.security.KeyStoreLoader;
import de.kernblick.opcuasimulator.server.security.ServerCertificateValidator;
import de.kernblick.opcuasimulator.server.security.TrustListManager;

public class OpcUaSimulatorServer {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private int TCP_BIND_PORT = 12686;
    private int HTTPS_BIND_PORT = 8443;

    private boolean tcpEnabled = true;
    private boolean httpsEnabled = true;

    private boolean authEnabled = true;
    private boolean discoveryEnabled = false;

    private static final String PUBLIC_ADDRESS = "0.0.0.0";
    private String BIND_ADDRESS = PUBLIC_ADDRESS;
    private String EXTERNAL_ADDRESS = PUBLIC_ADDRESS;

    private String USERNAME = "admin";
    private String PASSWORD = "admin";

    static {
        // Required for SecurityPolicy.Aes256_Sha256_RsaPss
        // Security.addProvider(new BouncyCastleProvider());

        try {
            NonceUtil.blockUntilSecureRandomSeeded(10, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private final OpcUaServer server;
    private final OpcUaSimulatorControlServerNamespace opcUaSimNamespace;

    private final List<ManagedNamespaceWithLifecycle> namespaces = new ArrayList<>();



    public OpcUaSimulatorServer(String bindAddress,
                                String externalAddress,
                                Integer bindPortTcp,
                                Integer bindPortHttps,
                                String nodeset2,
                                String username,
                                String password,
                                Boolean discovery) throws Exception {
        this(bindAddress, externalAddress, bindPortTcp, bindPortHttps, nodeset2, username, password, discovery, "0.0.0");
    }

    public OpcUaSimulatorServer(String bindAddress,
                                String externalAddress,
                                Integer bindPortTcp,
                                Integer bindPortHttps,
                                String nodeset2,
                                String username,
                                String password,
                                Boolean discovery,
                                String version) throws Exception {

        if(bindAddress != null && !bindAddress.isEmpty()) BIND_ADDRESS = bindAddress;

        if(externalAddress != null && !externalAddress.isEmpty()) EXTERNAL_ADDRESS = externalAddress;

        if (bindPortTcp != null) TCP_BIND_PORT = bindPortTcp.intValue();
        else tcpEnabled = false;

        if (bindPortHttps != null) HTTPS_BIND_PORT = bindPortHttps.intValue();
        else httpsEnabled = false;

        if(username == null || username.isBlank() || username.trim().equals("null") || username.trim().equals("false") || username.trim().equals("anonymous")) {
            authEnabled = false;
        } else {
            USERNAME = username;
            PASSWORD = password;
        }

        this.discoveryEnabled = discovery != null ? discovery : false;


        OpcUaServerConfigBuilder serverConfigBuilder = OpcUaServerConfig.builder()
            .setApplicationName(LocalizedText.english("KERNBLICK OPC UA Simulator"));

        // Increase Session Limit
        OpcUaServerConfigLimits opcUaServerConfigLimits = new OpcUaServerConfigLimits() {
            @Override
            public UInteger getMaxSessionCount() {
                return UInteger.valueOf(100_000);
            }
        };

        serverConfigBuilder.setLimits(opcUaServerConfigLimits);

        serverConfigBuilder = this.handleSecurity(serverConfigBuilder);


        OpcUaServerConfig serverConfig = serverConfigBuilder.build();

        // This also creates the namespace with URI KeyStoreLoader.APPLICATION_URI in ns=1
        server = new OpcUaServer(serverConfig);

        // create InputStreams from nodeset2 parameter
        InputStream[] nodeset2InputStreams = getNodeSet2InputSteams(nodeset2);

        for (InputStream inputStream : nodeset2InputStreams) {

            // parse NodeSet2 XML
            UANodeSet uaNodeSet = getUANodeSet(inputStream);

            if (uaNodeSet.getUAObjectOrUAVariableOrUAMethod().size() == 0) {
                LOGGER.error("NodeSet2 XML is empty");
                continue;
            }

            // get namespace index from first object
            UShort index = ushort(1);
            String nodeId = uaNodeSet.getUAObjectOrUAVariableOrUAMethod().get(0).getNodeId();
            if (nodeId != null && nodeId.startsWith("ns=")) {
                index = ushort(Integer.parseInt(nodeId.split("ns=")[1].substring(0, 1)));
            }

            // read namespace URI from NodeSet2 XML
            String namespaceUri = KeyStoreLoader.APPLICATION_URI;

            if (uaNodeSet.getNamespaceUris() != null
                && uaNodeSet.getNamespaceUris().getUri() != null
                && uaNodeSet.getNamespaceUris().getUri().size() > 0) {
                    namespaceUri = uaNodeSet.getNamespaceUris().getUri().get(0);
            }

            // create namespace
            server.getNamespaceTable().putUri(namespaceUri, index);
            ManagedNamespaceWithLifecycle namespace = new OpcUaSimulatorNodeSet2Namespace(server, namespaceUri, uaNodeSet);

            this.namespaces.add(namespace);
            namespace.startup();
        }

        opcUaSimNamespace = new OpcUaSimulatorControlServerNamespace(server, version);
        opcUaSimNamespace.startup();
    }

    /**
     * Creates input streams for the nodeset2 files defined.
     */
    private InputStream[] getNodeSet2InputSteams(String nodeset2) {

        List<InputStream> result = new ArrayList<>();

        // loop through nodesets and add if any
        if(!nodeset2.isBlank()) {
            String[] nodesets = nodeset2.split(",");
            for (String nodeset : nodesets) {
                nodeset = nodeset.trim();
                if(!nodeset.isBlank()) {

                    LOGGER.info("NodeSet2 " + nodeset + " shall be loaded.");

                    // search for nodeset in class's path, classloader's path, and current directory
                    InputStream nodeSetInputStream = getClass().getClassLoader().getResourceAsStream(nodeset);

                    // not found
                    if(nodeSetInputStream == null) {
                        nodeSetInputStream = getClass().getResourceAsStream(nodeset);
                    }

                    // still not found
                    if(nodeSetInputStream == null) {
                        try {
                            nodeSetInputStream = new FileInputStream(nodeset);
                        } catch (FileNotFoundException e) {
                            LOGGER.error("NodeSet2 file not found on classpath.");
                            throw new UaRuntimeException(new Exception("NodeSet2 file not found on classpath."));
                        }
                    }

                    result.add(nodeSetInputStream);

                }
            }
        } else {
            LOGGER.info("No nodeset added to address space.");
        }

        return result.toArray(new InputStream[0]);
    }

    /**
     * Returns the namespace URI that is defined in the nodeset2.
     * Therefore, the nodeset2 file is parsed already.
     * @throws DatatypeConfigurationException
     */
    private UANodeSet getUANodeSet(InputStream nodeSetXml) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        UANodeSet nodeSet = (UANodeSet) jaxbContext.createUnmarshaller().unmarshal(nodeSetXml);

        if(nodeSet.getModels() == null || nodeSet.getModels().getModel().size() < 1) {
            String message = "NodeSet2 file does not contain model entries.";
            LOGGER.warn(message);
            ModelTable modelTable = new ModelTable();
            ModelTableEntry modelTableEntry = new ModelTableEntry();
            modelTableEntry.setModelUri(KeyStoreLoader.APPLICATION_URI);
            modelTableEntry.setVersion("1.04");

            try {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(new Date());
                XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                modelTableEntry.setPublicationDate(date);
            } catch (DatatypeConfigurationException e) {
                e.printStackTrace();
            }

            modelTable.getModel().add(modelTableEntry);
            nodeSet.setModels(modelTable);
        }

        return nodeSet;
    }

    private BuildInfo getBuildInfo(String applicationUri) {
        return new BuildInfo(
                    applicationUri,
                    "KERNBLICK",
                    "KERNBLICK OPC UA Simulator",
                    OpcUaServer.SDK_VERSION,
                    "", DateTime.now());
    }

    private OpcUaServerConfigBuilder handleSecurity(OpcUaServerConfigBuilder serverConfigBuilder) throws Exception {

        Path securityTempDir = getSecurityTempDir();
        LOGGER.debug("Security dir: {}", securityTempDir.toAbsolutePath());

        File pkiDir = securityTempDir.resolve("pki").toFile();
        LOGGER.debug("Security pki dir: {}", pkiDir.getAbsolutePath());

        // retrieve server certificate (will be created if not existing)
        DefaultCertificateManager certificateManager = getCertificateManager(securityTempDir);
        X509Certificate certificate = getFirstCertificate(certificateManager);

        // retrieve trust manager to decide iff to trust client certificates
        // DefaultTrustListManager trustListManager = new DefaultTrustListManager(pkiDir);
        TrustListManager trustListManager = new TrustListManager(pkiDir);
        // DefaultServerCertificateValidator certificateValidator = new DefaultServerCertificateValidator(trustListManager);
        ServerCertificateValidator certificateValidator = new ServerCertificateValidator(trustListManager);

        // generate certificate for https channel
        KeyPair httpsKeyPair = SelfSignedCertificateGenerator.generateRsaKeyPair(2048);
        SelfSignedHttpsCertificateBuilder httpsCertificateBuilder = new SelfSignedHttpsCertificateBuilder(httpsKeyPair);

        // set certificate dns names
        httpsCertificateBuilder.setCommonName(HostnameUtil.getHostname());

        LOGGER.info("Getting hostnames for external address " + EXTERNAL_ADDRESS + " and for binding address " + BIND_ADDRESS);
        getHostnames(EXTERNAL_ADDRESS).forEach(httpsCertificateBuilder::addDnsName);
        getHostnames(BIND_ADDRESS).forEach(httpsCertificateBuilder::addDnsName);

        // Now let's build the certificate
        LOGGER.info("Building certificate.");
        X509Certificate httpsCertificate = httpsCertificateBuilder.build();

        // The configured application URI must match the one in the certificate(s)
        String applicationUri = CertificateUtil
            .getSanUri(certificate)
            .orElseThrow(() -> new UaRuntimeException(
                StatusCodes.Bad_ConfigurationError,
                "Certificate is missing the application URI"));

        Set<EndpointConfiguration> endpointConfigurations = createEndpointConfigurations(certificate);

        //* AUTHENTICATION
        // if username is null or empty, do not add validator.
        if (authEnabled) {
            UsernameIdentityValidator identityValidator = new UsernameIdentityValidator(
                true,
                authChallenge -> {
                    return USERNAME.equals(authChallenge.getUsername())
                        && PASSWORD.equals(authChallenge.getPassword());
                }
            );
            serverConfigBuilder.setIdentityValidator(identityValidator);
        }

        return serverConfigBuilder
            .setCertificateManager(certificateManager)
            .setTrustListManager(trustListManager)
            .setCertificateValidator(certificateValidator)
            .setHttpsKeyPair(httpsKeyPair)
            .setHttpsCertificateChain(new X509Certificate[] { httpsCertificate })
            .setApplicationUri(applicationUri)
            .setBuildInfo(getBuildInfo(applicationUri))
            .setProductUri(applicationUri)
            .setEndpoints(endpointConfigurations);

    }

    private static Map<String,Set<String>> hostnamesMap = new HashMap<>();
    public static Set<String> getHostnames(String address) {

        if(hostnamesMap.containsKey(address)) return hostnamesMap.get(address);

        Set<String> hostnames = new HashSet<>();
        InetAddress inetAddress;

        try {
            // get inet address
            inetAddress = InetAddress.getByName(address);

            // get network interface for provided IP
            NetworkInterface networkInterface = NetworkInterface.getByInetAddress(inetAddress);

            // now add hostnames of network interface to result list
            if(networkInterface != null) {
                Collections.list(networkInterface.getInetAddresses()).forEach(ia -> {
                    if (ia instanceof Inet4Address) {
                        hostnames.add(ia.getHostName());
                        hostnames.add(ia.getHostAddress());
                        hostnames.add(ia.getCanonicalHostName());
                    }
                });
            }

            // if network interface does not exist (e.g. IP is 0.0.0.0) try to get exterrnal IP and use local ip
            if(networkInterface == null) {
                hostnames.add(address);

                String externalIP = ExternalIPUtil.get();
                if (externalIP != null) {
                    InetAddress externalIA = InetAddress.getByName(externalIP);
                    hostnames.add(externalIA.getHostAddress());
                }

                InetAddress loopbackAddress = InetAddress.getLoopbackAddress();
                hostnames.add(loopbackAddress.getHostName());
                hostnames.add(loopbackAddress.getHostAddress());
                hostnames.add(loopbackAddress.getCanonicalHostName());

                InetAddress localHost = InetAddress.getLocalHost();
                hostnames.add(localHost.getHostName());
                hostnames.add(localHost.getHostAddress());
                hostnames.add(localHost.getCanonicalHostName());
            }


        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
         }

        if(hostnames.size() > 0) hostnamesMap.put(address, hostnames);
        return hostnames;
    }

    /**
     * Returns the security temp directory defined by java
     *
     * @return
     * @throws IOException
     * @throws Exception
     */
    private Path getSecurityTempDir() throws IOException, Exception {
        Path securityTempDir = Paths.get(System.getProperty("java.io.tmpdir"), "server", "security");
        Files.createDirectories(securityTempDir);
        if (!Files.exists(securityTempDir)) {
            throw new Exception("Unable to create security temp dir: " + securityTempDir);
        }
        return securityTempDir;
    }

    /**
     * Returns a certificate manager that holds/manages all certificates in given directory
     *
     * @param securityTempDir
     * @return
     * @throws Exception
     */
    private DefaultCertificateManager getCertificateManager(Path securityTempDir) throws Exception {
        KeyStoreLoader loader = new KeyStoreLoader().load(securityTempDir);

        return new DefaultCertificateManager(
            loader.getServerKeyPair(),
            loader.getServerCertificateChain()
        );
    }

    /**
     * Return the first certificate the manager has
     *
     * @param certificateManager
     * @return
     */
    private X509Certificate getFirstCertificate(DefaultCertificateManager certificateManager) {
        return certificateManager.getCertificates()
            .stream()
            .findFirst()
            .orElseThrow(() -> new UaRuntimeException(StatusCodes.Bad_ConfigurationError, "No certificate found"));
    }

    private Set<EndpointConfiguration> createEndpointConfigurations(X509Certificate certificate) {
        Set<EndpointConfiguration> endpointConfigurations = new LinkedHashSet<>();

        List<String> bindAddresses = newArrayList();
        bindAddresses.add(BIND_ADDRESS);

        Set<String> hostnames = new LinkedHashSet<>();

        if(EXTERNAL_ADDRESS.equals(PUBLIC_ADDRESS)) {
            hostnames.add(HostnameUtil.getHostname());
            hostnames.addAll(getHostnames(EXTERNAL_ADDRESS));
        } else {
            hostnames.add(EXTERNAL_ADDRESS);
        }

        for (String bindAddress : bindAddresses) {
            for (String hostname : hostnames) {
                EndpointConfiguration.Builder builder = EndpointConfiguration.newBuilder()
                    .setBindAddress(bindAddress)
                    .setHostname(hostname)
                    .setCertificate(certificate)
                    .addTokenPolicies(
                        authEnabled ? USER_TOKEN_POLICY_USERNAME : USER_TOKEN_POLICY_ANONYMOUS
                    );

                EndpointConfiguration.Builder noSecurityBuilder = builder.copy()
                    .setSecurityPolicy(SecurityPolicy.None)
                    .setSecurityMode(MessageSecurityMode.None);

                if(tcpEnabled) endpointConfigurations.add(buildTcpEndpoint(noSecurityBuilder));
                if(httpsEnabled) endpointConfigurations.add(buildHttpsEndpoint(noSecurityBuilder));

                // TCP Basic256Sha256 / SignAndEncrypt
                if(tcpEnabled)
                endpointConfigurations.add(buildTcpEndpoint(
                    builder.copy()
                        .setSecurityPolicy(SecurityPolicy.Basic256Sha256)
                        .setSecurityMode(MessageSecurityMode.SignAndEncrypt))
                );

                // HTTPS Basic256Sha256 / Sign (SignAndEncrypt not allowed for HTTPS)
                if(httpsEnabled)
                endpointConfigurations.add(buildHttpsEndpoint(
                    builder.copy()
                        .setSecurityPolicy(SecurityPolicy.Basic256Sha256)
                        .setSecurityMode(MessageSecurityMode.Sign))
                );

                /*
                 * It's good practice to provide a discovery-specific endpoint with no security.
                 * It's required practice if all regular endpoints have security configured.
                 *
                 * Usage of the  "/discovery" suffix is defined by OPC UA Part 6:
                 *
                 * Each OPC UA Server Application implements the Discovery Service Set. If the OPC UA Server requires a
                 * different address for this Endpoint it shall create the address by appending the path "/discovery" to
                 * its base address.
                 */

                if(discoveryEnabled) {
                    EndpointConfiguration.Builder discoveryBuilder = builder.copy()
                        .setPath("/discovery")
                        .setSecurityPolicy(SecurityPolicy.None)
                        .setSecurityMode(MessageSecurityMode.None);

                    if(tcpEnabled)   endpointConfigurations.add(buildTcpEndpoint(discoveryBuilder));
                    if(httpsEnabled) endpointConfigurations.add(buildHttpsEndpoint(discoveryBuilder));
                }
            }
        }

        return endpointConfigurations;
    }

    private EndpointConfiguration buildTcpEndpoint(EndpointConfiguration.Builder base) {
        return base.copy()
            .setTransportProfile(TransportProfile.TCP_UASC_UABINARY)
            .setBindPort(this.TCP_BIND_PORT)
            .build();
    }

    private EndpointConfiguration buildHttpsEndpoint(EndpointConfiguration.Builder base) {
        return base.copy()
            .setTransportProfile(TransportProfile.HTTPS_UABINARY)
            .setBindPort(this.HTTPS_BIND_PORT)
            .build();
    }

    public OpcUaSimulatorServer tcpBindPort(int port) {
        this.TCP_BIND_PORT = port;
        return this;
    }

    public OpcUaSimulatorServer httpsBindPort(int port) {
        this.HTTPS_BIND_PORT = port;
        return this;
    }

    public OpcUaServer getServer() {
        return server;
    }

    public CompletableFuture<OpcUaServer> startup() {
        return server.startup();
    }

    public CompletableFuture<OpcUaServer> shutdown() {
        opcUaSimNamespace.shutdown();

        for (ManagedNamespaceWithLifecycle managedNamespaceWithLifecycle : namespaces) {
            managedNamespaceWithLifecycle.shutdown();
        }

        return server.shutdown();
    }

}

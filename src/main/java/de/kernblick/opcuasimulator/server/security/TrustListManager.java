package de.kernblick.opcuasimulator.server.security;

import java.io.File;
import java.io.IOException;
import java.security.cert.X509Certificate;

import org.eclipse.milo.opcua.stack.core.security.DefaultTrustListManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrustListManager extends DefaultTrustListManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrustListManager.class);

    public TrustListManager(File baseDir) throws IOException {
        super(baseDir);
    }

    @Override
    public synchronized void addRejectedCertificate(X509Certificate certificate) {
        this.addTrustedCertificate(certificate);
        LOGGER.info("Added certificate to trust store: {}", certificate.getSubjectX500Principal().toString());
        LOGGER.debug("WARN: All certificates are always added to the trust store.");
    }
}

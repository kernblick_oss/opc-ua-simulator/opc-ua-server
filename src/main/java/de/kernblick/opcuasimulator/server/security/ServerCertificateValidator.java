package de.kernblick.opcuasimulator.server.security;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.security.TrustListManager;
import org.eclipse.milo.opcua.stack.core.util.validation.ValidationCheck;
import org.eclipse.milo.opcua.stack.server.security.DefaultServerCertificateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This stub validator validates all certificate chains positively
 */
public class ServerCertificateValidator extends DefaultServerCertificateValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerCertificateValidator.class);

    public ServerCertificateValidator(TrustListManager trustListManager) {
        super(trustListManager);
    }

    public ServerCertificateValidator(TrustListManager trustListManager,Set<ValidationCheck> validationChecks) {
        super(trustListManager, validationChecks);
    }

    @Override
    public void validateCertificateChain(List<X509Certificate> certificateChain) throws UaException {
        String name = certificateChain.get(0).getIssuerX500Principal().getName();
        LOGGER.info("Accepting certificate without checks: {}", name != null ? name : "No name given.");
        LOGGER.debug("WARN: All certificates are always validated as positive with real checks.");
        return;
    }

    @Override
    public void validateCertificateChain(List<X509Certificate> certificateChain, String applicationUri) throws UaException {
        this.validateCertificateChain(certificateChain);
    }

}

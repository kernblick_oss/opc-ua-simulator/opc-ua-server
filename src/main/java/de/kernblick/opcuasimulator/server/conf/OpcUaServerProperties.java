package de.kernblick.opcuasimulator.server.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "opcua-simulator-server")
public class OpcUaServerProperties {

    /**
     * Bind IP of OPC UA server. Use 0.0.0.0 if you want to listen on all interfaces.
     */
    private String bindAddress;

    /**
     * External IP of OPC UA server. Use 0.0.0.0 if you want to listen on all interfaces.
     */
    private String externalAddress;

    /**
     * TCP Port to listen to. Disables TCP if set to null.
     */
    private String bindPortTcp;

    /**
     * HTTPS Port to listen to. Disables HTTPS if set to null.
     */
    private String bindPortHttps;

    /**
     * File name of NodeSet2 XML.
     */
    private String nodeset2;

    /**
     * Username for logging into OPC UA server.
     */
    private String username;

    /**
     * Password for logging into OPC UA server.
     */
    private String password;

    /**
     * Enable discovery endpoint.
     */
    private Boolean discovery;

    /**
     * Version of server that is shown in ControlServer namespace
     */
    private String version;

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean isDiscovery() {
        return this.discovery;
    }

    public Boolean getDiscovery() {
        return this.discovery;
    }

    public void setDiscovery(Boolean discovery) {
        this.discovery = discovery;
    }

    public String getBindAddress() {
        return this.bindAddress;
    }

    public void setBindAddress(String bindAddress) {
        this.bindAddress = bindAddress;
    }

    public String getExternalAddress() {
        return this.externalAddress;
    }

    public void setExternalAddress(String externalAddress) {
        this.externalAddress = externalAddress;
    }

    public String getBindPortTcp() {
        return this.bindPortTcp;
    }

    public void setBindPortTcp(String bindPortTcp) {
        this.bindPortTcp = bindPortTcp;
    }

    public String getBindPortHttps() {
        return this.bindPortHttps;
    }

    public void setBindPortHttps(String bindPortHttps) {
        this.bindPortHttps = bindPortHttps;
    }

    public String getNodeset2() {
        return this.nodeset2;
    }

    public void setNodeset2(String nodeset2) {
        this.nodeset2 = nodeset2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}

package de.kernblick.opcuasimulator.server.conf;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.kernblick.opcuasimulator.server.OpcUaSimulatorServer;

@Configuration
public class OpcUaServerConf {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Bean("opcua-simulator-server")
	public OpcUaSimulatorServer opcUaSimulatorServer(
		@Value("${opcua-simulator-server.bind-address:localhost}") String bindAddress,
		@Value("${opcua-simulator-server.external-address:localhost}") String externalAddress,
		@Value("${opcua-simulator-server.bind-port-tcp:4840}") Integer bindPortTcp,
		@Value("${opcua-simulator-server.bind-port-https}") Integer bindPortHttps,
		@Value("${opcua-simulator-server.nodeset2:OPCUAServer.NodeSet2.xml}") String nodeset2,
		@Value("${opcua-simulator-server.username:admin}") String username,
		@Value("${opcua-simulator-server.password:admin}") String password,
		@Value("${opcua-simulator-server.discovery:false}") Boolean discovery,
		@Value("${opcua-simulator-server.version:1.0}") String version
	) throws InterruptedException, ExecutionException {

		LOGGER.info(String.format("Starting OPC UA server v%s with the following arguments:", version));
		LOGGER.info(String.format(" - Bind Address: %s", bindAddress));
		LOGGER.info(String.format(" - External Address: %s", externalAddress));
		LOGGER.info(String.format(" - TCP Port: %s", bindPortTcp != null ? bindPortTcp : "disabled"));
		LOGGER.info(String.format(" - HTTPS Port: %s", bindPortHttps != null ? bindPortHttps : "disabled"));
		LOGGER.info(String.format(" - NodeSet2: %s", nodeset2));

		if(discovery) LOGGER.info(String.format(" - Discovery endpoint enabled."));

		if(username != null && !username.isBlank()) {
			LOGGER.info(String.format(" - Username: %s", username));

			if (password.length() > 5) LOGGER.info(String.format(" - Password: %s...", password.substring(0, 3)));
			else                       LOGGER.info(String.format(" - Password: ..."));
		} else {
			LOGGER.info(String.format(" - Authentication: anonymous"));
		}

		try {
			OpcUaSimulatorServer server = new OpcUaSimulatorServer(
												bindAddress,
												externalAddress,
												bindPortTcp,
												bindPortHttps,
												nodeset2,
												username,
												password,
												discovery != null && discovery ? true : false,
												version);
			return server;
		} catch(Exception ex) {
			LOGGER.error(ex.getLocalizedMessage());
			return null;
		}

	}

}

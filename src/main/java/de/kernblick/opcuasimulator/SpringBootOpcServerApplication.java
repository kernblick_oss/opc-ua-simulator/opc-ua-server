package de.kernblick.opcuasimulator;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import de.kernblick.opcuasimulator.server.OpcUaSimulatorServer;

@SpringBootApplication
@EnableScheduling
public class SpringBootOpcServerApplication {
	
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
        if (args.length>0 && (args[0].contains("help") || args[0].contains("?"))) {
            System.out.println("                            ");
            System.out.println("OPC UA Server");
            System.out.println("============================");
            System.out.println("Use the following environment variables to configure the server:");
			System.out.println(" - BIND_ADDRESS: The bind address. Default: 0.0.0.0");
			System.out.println(" - EXTERNAL_ADDRESS: The external address the server will be accessed via. Default: 0.0.0.0");
			System.out.println(" - BIND_PORT_TCP: The TCP bind port for the OPC UA server. Default: 4840");
			System.out.println(" - BIND_PORT_HTTPS: The HTTPS bind port for the OPC UA server. Default: disabled");
			System.out.println(" - AUTH_USERNAME: The username to use for authentication. If it contains 'anon', no user will be used for authentication. Default: admin");
			System.out.println(" - AUTH_PASSWORD: The password to use for authentication. Default: admin");
			System.out.println(" - NODESET2: The path to the nodeset2 file. Default: A bundled example nodeset2 file is used.");
            System.out.println("                            ");
        } else {
			//Start Spring Boot Application to create IoT Container
			SpringApplication.run(SpringBootOpcServerApplication.class, args);
        }
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			
			try {

				// Get Simple OpcUa Server from IoT container
				OpcUaSimulatorServer simulationServer = (OpcUaSimulatorServer) ctx.getBean("opcua-simulator-server");

				// Start Simple OpcUa Server
				simulationServer.startup().get();

				// Run endless
				final CompletableFuture<Void> future = new CompletableFuture<>();
				Runtime.getRuntime().addShutdownHook(new Thread(() -> future.complete(null)));
				future.get();

			} catch (Exception ex) {

				LOGGER.error("Could not start Simulation Server");
				LOGGER.debug(ex.getLocalizedMessage(), ex);

			}

		};
	}



}
/*
 * Copyright (c) 2021 Kevin Herron
 * SPDX-License-Identifier: Apache-2.0
 */

package de.kernblick.opcuasimulator.nodeset.util;

import static java.util.stream.Collectors.groupingBy;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.google.common.collect.ListMultimap;

// import org.opcfoundation.ua.generated.Reference;
import org.eclipse.milo.opcua.sdk.core.Reference;
import org.eclipse.milo.opcua.sdk.core.ValueRanks;
import org.eclipse.milo.opcua.sdk.server.UaNodeManager;
import org.eclipse.milo.opcua.sdk.server.api.NodeManager;
import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaNodeContext;
import org.eclipse.milo.opcua.sdk.server.nodes.UaVariableNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.types.builtin.ExtensionObject;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.XmlElement;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.structured.Argument;
import org.jetbrains.annotations.Nullable;
import org.opcfoundation.ua.generated.UAArgument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kernblick.opcuasimulator.nodeset.UaNodeSet;
import de.kernblick.opcuasimulator.nodeset.attributes.DataTypeNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.MethodNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.NodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.ObjectNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.ObjectTypeNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.ReferenceTypeNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.VariableNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.VariableTypeNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.attributes.ViewNodeAttributes;
import de.kernblick.opcuasimulator.nodeset.methods.MethodCallEventHandler;


public class NodeSetUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeSetUtils.class);
    // private static final SerializationContext SERIALIZATION_CONTEXT = new SerializationContext() {

    //     private final NamespaceTable namespaceTable = new NamespaceTable();

    //     @Override
    //     public EncodingLimits getEncodingLimits() {
    //         return EncodingLimits.DEFAULT;
    //     }

    //     @Override
    //     public NamespaceTable getNamespaceTable() {
    //         return namespaceTable;
    //     }

    //     @Override
    //     public DataTypeManager getDataTypeManager() {
    //         return OpcUaDataTypeManager.getInstance();
    //     }
    // };

    /**
     * Parse a NodeSet that has been parsed from XML and add nodes to NodeManager
     *
     * @param nodeSet
     * @param context
     * @param nodeManager
     */
    public static void parseNodeSet(UaNodeSet nodeSet, UaNodeContext context, UaNodeManager nodeManager){

        Map<NodeId, NodeAttributes> nodes = nodeSet.getNodes();
        ListMultimap<NodeId, Reference> allReferences = nodeSet.getCombinedReferences();

        Map<NodeClass, List<NodeAttributes>> collect = nodes.values().stream()
                .collect(groupingBy(NodeAttributes::getNodeClass));

        Map<MethodNodeAttributes, UaMethodNode> methods = new HashMap<MethodNodeAttributes, UaMethodNode>();

		// Knoten in nodeManager nach verschiedenen Kategorien hinzufügen
        for (NodeClass nodeClass : collect.keySet()) {
            if (nodeClass.equals(NodeClass.ObjectType)) {
                collect.get(NodeClass.ObjectType).stream()
                .map(node -> ((ObjectTypeNodeAttributes) node))
                .map(node -> node.getUaObjectTypeNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.ObjectType).size() + " ObjectTypes loaded.");
            } else if (nodeClass.equals(NodeClass.VariableType)) {
                collect.get(NodeClass.VariableType).stream()
                .map(node -> ((VariableTypeNodeAttributes) node))
                .map(node -> node.getUaVariableTypeNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.VariableType).size() + " VariableTypes loaded.");
            } else if (nodeClass.equals(NodeClass.Object)) {
                collect.get(NodeClass.Object).stream()
                .map(node -> ((ObjectNodeAttributes) node))
                .map(node -> node.getObjectNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.Object).size() + " Objects loaded.");
            } else if (nodeClass.equals(NodeClass.Variable)) {
                collect.get(NodeClass.Variable).stream()
                .map(node -> ((VariableNodeAttributes) node))
                .map(node -> node.getUaVariableNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.Variable).size() + " Variables loaded.");
            } else if (nodeClass.equals(NodeClass.Method)) {

                List<NodeAttributes> attributesList = collect.get(NodeClass.Method);
                for (NodeAttributes nodeAttributes : attributesList) {
                    MethodNodeAttributes methodNodeAttributes = (MethodNodeAttributes)nodeAttributes;
                    UaMethodNode methodNode = methodNodeAttributes.getUaMethodNode(context);

                    methods.put(methodNodeAttributes, methodNode);
                    nodeManager.addNode(methodNode);
                }
                LOGGER.info(collect.get(NodeClass.Method).size() + " Methods loaded.");
            } else if (nodeClass.equals(NodeClass.DataType)) {
                collect.get(NodeClass.DataType).stream()
                .map(node -> ((DataTypeNodeAttributes) node))
                .map(node -> node.getDataTypeNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.DataType).size() + " DataTypes loaded.");
            } else if (nodeClass.equals(NodeClass.View)) {
                collect.get(NodeClass.View).stream()
                .map(node -> ((ViewNodeAttributes) node))
                .map(node -> node.getUaViewNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.View).size() + " Views loaded.");
            } else if (nodeClass.equals(NodeClass.ReferenceType)) {
                collect.get(NodeClass.ReferenceType).stream()
                .map(node -> ((ReferenceTypeNodeAttributes) node))
                .map(node -> node.getReferenceTypeNode(context))
                .forEach(nodeManager::addNode);
                LOGGER.info(collect.get(NodeClass.ReferenceType).size() + " ReferenceTypes loaded.");
            }
        }

		// Alle Referenzen in nodeManager hinzufügen
        allReferences.values()
                .forEach(nodeManager::addReference);

        // Create method Handlers for Methods
        for (Map.Entry<MethodNodeAttributes, UaMethodNode> entry : methods.entrySet()) {
            MethodNodeAttributes methodNodeAttributes = entry.getKey();
            UaMethodNode methodNode = entry.getValue();

            registerMethodHandler(methodNodeAttributes, methodNode, context);
        }

    }

    private static void registerMethodHandler(MethodNodeAttributes methodAttributes, UaMethodNode methodNode, UaNodeContext context) {

        /**
         * Create MethodCallEvent that can be observed in the server.
         * By this, calls to methods defined in the NodeSet2 can be monitored.
         */
        MethodCallEventHandler methodCallEventHandler = new MethodCallEventHandler(methodNode, methodAttributes);

        for (org.opcfoundation.ua.generated.Reference reference : methodAttributes.getProperties()) {
            NodeManager<UaNode> nodeManager = context.getNodeManager();

            @Nullable
            UaNode referenceNodes = nodeManager.get(NodeId.parse(reference.getValue()));

            if (referenceNodes instanceof UaVariableNode && referenceNodes.getBrowseName().getName().equals("InputArguments")) {
                UaVariableNode inputArguments = (UaVariableNode) referenceNodes;

                List<UAArgument> arguments = extractArguments(inputArguments);
                for (UAArgument uaArgument : arguments) {
                    Argument argument = createArgument(uaArgument, context);
                    methodAttributes.addInputArgument( argument );
                    methodCallEventHandler.addInputArgument( argument );
                }

            } else if (referenceNodes instanceof UaVariableNode && referenceNodes.getBrowseName().getName().equals("OutputArguments")) {
                UaVariableNode outputArguments = (UaVariableNode) referenceNodes;

                List<UAArgument> arguments = extractArguments(outputArguments);
                for (UAArgument uaArgument : arguments) {
                    Argument argument = createArgument(uaArgument, context);
                    methodAttributes.addOutputArgument( argument );
                    methodCallEventHandler.addOutputArgument( argument );
                }
            }
        }

        methodNode.setInvocationHandler(methodCallEventHandler);
    }

    private static NodeId getNodeId(UAArgument uaArgument) {
        NodeId nodeId = NodeId.parse(uaArgument.getDataType().getValue().getIdentifier().getValue());

        if (nodeId.getNamespaceIndex().intValue() == 0) {
            return nodeId;
        } else {
            return Identifiers.Int32; // TODO Int32 is used for enumeration values. Check for real DataType here.
        }
    }

    private static org.eclipse.milo.opcua.stack.core.types.structured.Argument createArgument(UAArgument uaArgument, UaNodeContext context) {
        NodeId nodeId = getNodeId(uaArgument);

        return new org.eclipse.milo.opcua.stack.core.types.structured.Argument(
            uaArgument.getName().getValue(),
            nodeId,
            ValueRanks.Scalar,
            null,
            new LocalizedText(uaArgument.getDescription().getValue().getText().getValue())
        );
    }

    @Nullable
    private static List<UAArgument> extractArguments(UaVariableNode variableNode ) {

        List<UAArgument> result = new ArrayList<UAArgument>();

        // check if input arguments are defined as ExtensionObjects
        if (variableNode.getValue().getValue().getValue() instanceof ExtensionObject[]) {
            ExtensionObject[] extensionObjects = (ExtensionObject[]) variableNode.getValue().getValue().getValue();

            for (ExtensionObject extensionObject : extensionObjects) {
                if (extensionObject.getBodyType().name().equals("XmlElement")) {
                    XmlElement value = (XmlElement) extensionObject.getBody();

                    try {
                        JAXBContext jaxbContext = JAXBContext.newInstance(UAArgument.class);
                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

                        StringReader reader = new StringReader(value.getFragment());
                        JAXBElement jaxbElement = (JAXBElement) jaxbUnmarshaller.unmarshal(reader);

                        if(jaxbElement.getValue() instanceof UAArgument) {
                            result.add( (UAArgument) jaxbElement.getValue() );
                        }

                    } catch (JAXBException e) {
                        e.printStackTrace();
                        return null;
                    }

                }
            }
        }

        return result;
    }
}
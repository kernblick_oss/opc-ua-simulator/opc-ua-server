/*
 * Copyright (c) 2021 Kevin Herron
 * SPDX-License-Identifier: Apache-2.0
 */

package de.kernblick.opcuasimulator.nodeset.attributes;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

import org.eclipse.milo.opcua.sdk.server.nodes.UaNodeContext;
import org.eclipse.milo.opcua.sdk.server.nodes.UaObjectTypeNode;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.opcfoundation.ua.generated.UAObjectType;

public class ObjectTypeNodeAttributes extends NodeAttributes {

    private final boolean isAbstract;

    public ObjectTypeNodeAttributes(
        NodeId nodeId,
        QualifiedName browseName,
        LocalizedText displayName,
        LocalizedText description,
        UInteger writeMask,
        UInteger userWriteMask,
        boolean isAbstract
    ) {

        super(nodeId, NodeClass.ObjectType, browseName, displayName, description, writeMask, userWriteMask);

        this.isAbstract = isAbstract;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    @Override
    public String toString() {
        return "ObjectTypeNodeAttributes{" +
            "isAbstract=" + isAbstract +
            "} " + super.toString();
    }

    public static ObjectTypeNodeAttributes fromGenerated(UAObjectType gNode) {
        NodeId nodeId = NodeId.parse(gNode.getNodeId());
        QualifiedName browseName = QualifiedName.parse(gNode.getBrowseName());

        LocalizedText displayName = gNode.getDisplayName().stream()
            .findFirst()
            .map(gLocalizedText -> LocalizedText.english(gLocalizedText.getValue()))
            .orElse(LocalizedText.english(browseName.getName()));

        LocalizedText description = gNode.getDescription().stream()
            .findFirst()
            .map(gLocalizedText -> LocalizedText.english(gLocalizedText.getValue()))
            .orElse(LocalizedText.NULL_VALUE);

        UInteger writeMask = uint(gNode.getWriteMask());
        UInteger userWriteMask = uint(gNode.getUserWriteMask());

        boolean isAbstract = gNode.isIsAbstract();

        return new ObjectTypeNodeAttributes(
            nodeId,
            browseName,
            displayName,
            description,
            writeMask,
            userWriteMask,
            isAbstract
        );
    }

    public UaObjectTypeNode getUaObjectTypeNode(UaNodeContext context) {
        return new UaObjectTypeNode(
                context,
                getNodeId(),
                getBrowseName(),
                getDisplayName(),
                getDescription(),
                getWriteMask(),
                getUserWriteMask(),
                isAbstract()
        );
    }

}

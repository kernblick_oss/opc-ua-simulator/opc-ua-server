/*
 * Copyright (c) 2021 Kevin Herron
 * SPDX-License-Identifier: Apache-2.0
 */

package de.kernblick.opcuasimulator.nodeset.attributes;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaNodeContext;
import org.eclipse.milo.opcua.stack.core.BuiltinReferenceType;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.eclipse.milo.opcua.stack.core.types.structured.Argument;
import org.opcfoundation.ua.generated.Reference;
import org.opcfoundation.ua.generated.UAMethod;

public class MethodNodeAttributes extends NodeAttributes {

    private final boolean executable;
    private final boolean userExecutable;

    private final List<Reference> properties;

    private List<Argument> inputArguments;
    private List<Argument> outputArguments;


    public MethodNodeAttributes(
        NodeId nodeId,
        QualifiedName browseName,
        LocalizedText displayName,
        LocalizedText description,
        UInteger writeMask,
        UInteger userWriteMask,
        boolean executable,
        boolean userExecutable,
        List<Reference> properties
    ) {

        super(nodeId, NodeClass.Method, browseName, displayName, description, writeMask, userWriteMask);

        this.executable = executable;
        this.userExecutable = userExecutable;
        this.properties = properties;

        inputArguments = new ArrayList<Argument>();
        outputArguments = new ArrayList<Argument>();

    }

    public boolean isExecutable() {
        return executable;
    }

    public boolean isUserExecutable() {
        return userExecutable;
    }

    public List<Reference> getProperties() {
        return this.properties;
    }


    public List<Argument> getInputArguments() {
        return this.inputArguments;
    }

    public void setInputArguments(List<Argument> inputArguments) {
        this.inputArguments = inputArguments;
    }

    public void addInputArgument(Argument inputArgument) {
        this.inputArguments.add(inputArgument);
    }

    public List<Argument> getOutputArguments() {
        return this.outputArguments;
    }

    public void setOutputArguments(List<Argument> outputArguments) {
        this.outputArguments = outputArguments;
    }

    public void addOutputArgument(Argument outputArgument) {
        this.outputArguments.add(outputArgument);
    }


    @Override
    public String toString() {
        return "MethodNodeAttributes{" +
            "executable=" + executable +
            ", userExecutable=" + userExecutable +
            "} " + super.toString();
    }

    public static MethodNodeAttributes fromGenerated(UAMethod gNode) {
        NodeId nodeId = NodeId.parse(gNode.getNodeId());
        QualifiedName browseName = QualifiedName.parse(gNode.getBrowseName());

        LocalizedText displayName = gNode.getDisplayName().stream()
            .findFirst()
            .map(gLocalizedText -> LocalizedText.english(gLocalizedText.getValue()))
            .orElse(LocalizedText.english(browseName.getName()));

        LocalizedText description = gNode.getDescription().stream()
            .findFirst()
            .map(gLocalizedText -> LocalizedText.english(gLocalizedText.getValue()))
            .orElse(LocalizedText.NULL_VALUE);

        UInteger writeMask = uint(gNode.getWriteMask());
        UInteger userWriteMask = uint(gNode.getUserWriteMask());

        boolean executable = gNode.isExecutable();
        boolean userExecutable = gNode.isUserExecutable();

        List<Reference> properties = new ArrayList<Reference>();
        if (gNode.getReferences() != null) {
            List<Reference> references = gNode.getReferences().getReference();

            for (Reference reference : references) {
                if (reference.getReferenceType().equals(BuiltinReferenceType.HasProperty.getBrowseName().getName())) {
                    properties.add(reference);
                }
            }
        }

        return new MethodNodeAttributes(
            nodeId,
            browseName,
            displayName,
            description,
            writeMask,
            userWriteMask,
            executable,
            userExecutable,
            properties
        );
    }

    public UaMethodNode getUaMethodNode(UaNodeContext context) {

        UaMethodNode methodNode = UaMethodNode.builder(context)
            .setNodeId(getNodeId())
            .setBrowseName(getBrowseName())
            .setDisplayName(getDisplayName())
            .setDescription(getDescription())
            .setUserExecutable(isUserExecutable())
            .setExecutable(isExecutable())
            .setWriteMask(getWriteMask())
            .setUserWriteMask(getUserWriteMask())
            .build();

        return methodNode;
    }


}

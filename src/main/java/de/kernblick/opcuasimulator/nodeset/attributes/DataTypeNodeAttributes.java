/*
 * Copyright (c) 2021 Kevin Herron
 * SPDX-License-Identifier: Apache-2.0
 */

package de.kernblick.opcuasimulator.nodeset.attributes;

import org.eclipse.milo.opcua.stack.core.BuiltinReferenceType;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.NodeClass;
import org.opcfoundation.ua.generated.UADataType;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.milo.opcua.sdk.server.nodes.UaDataTypeNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaNodeContext;
import org.opcfoundation.ua.generated.Reference;

public class DataTypeNodeAttributes extends NodeAttributes {

    private final boolean isAbstract;

    private final List<Reference> properties;

    public DataTypeNodeAttributes(
        NodeId nodeId,
        QualifiedName browseName,
        LocalizedText displayName,
        LocalizedText description,
        UInteger writeMask,
        UInteger userWriteMask,
        boolean isAbstract,
        List<Reference> properties
    ) {

        super(nodeId, NodeClass.DataType, browseName, displayName, description, writeMask, userWriteMask);

        this.isAbstract = isAbstract;
        this.properties = properties;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public List<Reference> getProperties() {
        return this.properties;
    }

    @Override
    public String toString() {
        return "DataTypeNodeAttributes{" +
            "isAbstract=" + isAbstract +
            "} " + super.toString();
    }

    public static DataTypeNodeAttributes fromGenerated(UADataType gNode) {
        NodeId nodeId = NodeId.parse(gNode.getNodeId());
        QualifiedName browseName = QualifiedName.parse(gNode.getBrowseName());

        LocalizedText displayName = gNode.getDisplayName().stream()
            .findFirst()
            .map(gLocalizedText -> LocalizedText.english(gLocalizedText.getValue()))
            .orElse(LocalizedText.english(browseName.getName()));

        LocalizedText description = gNode.getDescription().stream()
            .findFirst()
            .map(gLocalizedText -> LocalizedText.english(gLocalizedText.getValue()))
            .orElse(LocalizedText.NULL_VALUE);

        UInteger writeMask = uint(gNode.getWriteMask());
        UInteger userWriteMask = uint(gNode.getUserWriteMask());

        boolean isAbstract = gNode.isIsAbstract();

        List<Reference> properties = new ArrayList<Reference>();
        if (gNode.getReferences() != null) {
            List<Reference> references = gNode.getReferences().getReference();

            for (Reference reference : references) {
                if (reference.getReferenceType().equals(BuiltinReferenceType.HasProperty.getBrowseName().getName())) {
                    properties.add(reference);
                }
            }
        }

        return new DataTypeNodeAttributes(
            nodeId,
            browseName,
            displayName,
            description,
            writeMask,
            userWriteMask,
            isAbstract,
            properties
        );
    }

    public UaDataTypeNode getDataTypeNode(UaNodeContext context) {
        return new UaDataTypeNode(
                context,
                getNodeId(),
                getBrowseName(),
                getDisplayName(),
                getDescription(),
                getWriteMask(),
                getUserWriteMask(),
                isAbstract()
        );
    }

}

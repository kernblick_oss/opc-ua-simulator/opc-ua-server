package de.kernblick.opcuasimulator.nodeset.methods;


import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Argument")
@XmlAccessorType (XmlAccessType.FIELD)
class MethodArgument {

    @XmlAttribute
    String name;

    @XmlAttribute
    String value;

    public MethodArgument() {}

    public MethodArgument(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }

}

@XmlRootElement(name="MethodCall")
@XmlAccessorType (XmlAccessType.FIELD)
public class MethodCallEventModel {

    @XmlElement(name="BrowseName")
    String browseName;

    @XmlElement(name="NodeId")
    String nodeId;

    @XmlElement(name="DisplayName")
    String displayName;

    @XmlElementWrapper(name = "Arguments")
    @XmlElement(name="Argument")
    List<MethodArgument> arguments;

    public String getBrowseName() {
        return this.browseName;
    }

    public void setBrowseName(String browseName) {
        this.browseName = browseName;
    }

    public String getNodeId() {
        return this.nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<MethodArgument> getArguments() {
        return this.arguments;
    }

    public void setArguments(List<MethodArgument> arguments) {
        this.arguments = arguments;
    }

}

/*
 * Copyright (c) 2019 the Eclipse Milo Authors
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package de.kernblick.opcuasimulator.nodeset.methods;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.ushort;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.methods.AbstractMethodInvocationHandler;
import org.eclipse.milo.opcua.sdk.server.model.nodes.objects.BaseEventTypeNode;
import org.eclipse.milo.opcua.sdk.server.nodes.UaMethodNode;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.structured.Argument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kernblick.opcuasimulator.nodeset.attributes.MethodNodeAttributes;

public class MethodCallEventHandler extends AbstractMethodInvocationHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final OpcUaServer server;

    private UaMethodNode methodNode;
    private MethodNodeAttributes methodNodeAttributes;

    private Argument[] inputArguments;

    public void setInputArguments(Argument[] inputArguments) {
        this.inputArguments = inputArguments;
    }

    public void setOutputArguments(Argument[] outputArguments) {
        this.outputArguments = outputArguments;
    }

    public void addInputArgument(Argument inputArgument) {
        ArrayList<Argument> tmp = new ArrayList<Argument>(Arrays.asList(getInputArguments()));
        tmp.add(inputArgument);
        this.inputArguments = tmp.toArray(new Argument[tmp.size()]);
    }

    public void addOutputArgument(Argument outputArgument) {
        ArrayList<Argument> tmp = new ArrayList<Argument>(Arrays.asList(getOutputArguments()));
        tmp.add(outputArgument);
        this.outputArguments = tmp.toArray(new Argument[tmp.size()]);
    }

    private Argument[] outputArguments;

    public MethodCallEventHandler(UaMethodNode methodNode, MethodNodeAttributes methodNodeAttributes) {
        super(methodNode);

        this.server = methodNode.getNodeContext().getServer();
        this.methodNode = methodNode;
        this.methodNodeAttributes = methodNodeAttributes;
    }

    @Override
    public Argument[] getInputArguments() {
        return inputArguments != null ? inputArguments : new Argument[0];
    }

    @Override
    public Argument[] getOutputArguments() {
        return outputArguments != null ? outputArguments : new Argument[0];
    }

    @Override
    protected Variant[] invoke(InvocationContext invocationContext, Variant[] inputValues) throws UaException {

        BaseEventTypeNode eventNode = server.getEventFactory().createEvent(
            new NodeId(1, UUID.randomUUID()),
            Identifiers.BaseEventType
        );

        // Create method data as event payload
        MethodCallEventModel model = new MethodCallEventModel();
        model.setBrowseName(this.methodNode.getBrowseName().toParseableString());
        model.setDisplayName(getNode().getDisplayName().getText());
        model.setNodeId(getNode().getNodeId().toParseableString());

        // check input arguments
        if (inputValues.length != methodNodeAttributes.getInputArguments().size()) {
            logger.error("Input arguments do not have the correct length");
            return new Variant[0];
        }

        Argument[] inputArguments = methodNodeAttributes.getInputArguments().toArray(new Argument[methodNodeAttributes.getInputArguments().size()]);

        // loop through arguments and add to XML
        ArrayList<MethodArgument> args = new ArrayList<MethodArgument>();
        for (int i = 0; i < inputArguments.length; i++) {
            args.add(
                new MethodArgument(
                    inputArguments[i].getName(),
                    inputValues[i].getValue().toString()
                )
            );
        }
        model.setArguments(args);

        // Create XML for data
        StringWriter sw = new StringWriter();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(MethodCallEventModel.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(model, sw);
            // jaxbMarshaller.marshal(model, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        String message = sw.toString();

        eventNode.setBrowseName(this.methodNode.getBrowseName());
        eventNode.setDisplayName(this.methodNode.getDisplayName());
        eventNode.setEventId(ByteString.of(new byte[]{0, 1, 2, 3}));
        eventNode.setEventType(this.methodNode.getNodeId());
        eventNode.setSourceNode(getNode().getNodeId());
        eventNode.setSourceName(getNode().getDisplayName().getText());
        eventNode.setTime(DateTime.now());
        eventNode.setReceiveTime(DateTime.NULL_VALUE);
        eventNode.setMessage(LocalizedText.english(message));
        eventNode.setSeverity(ushort(2));

        server.getEventBus().post(eventNode);
        eventNode.delete();

        return new Variant[0];
    }

}
<?xml version="1.0" encoding="utf-8" ?>
<opc:ModelDesign
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema"
xmlns:opc="http://opcfoundation.org/UA/ModelDesign.xsd"
xmlns:ua="http://opcfoundation.org/UA/"
xmlns:uax="http://opcfoundation.org/UA/2008/02/Types.xsd"
xmlns="http://opcfoundation.org/OPCUAServer"
TargetNamespace="http://opcfoundation.org/OPCUAServer">

	<opc:Namespaces>
		<opc:Namespace Name="OpcUa" Prefix="Opc.Ua" XmlNamespace="http://opcfoundation.org/UA/2008/02/Types.xsd">http://opcfoundation.org/UA/</opc:Namespace>
		<opc:Namespace Name="OPCUAServer" Prefix="OPCUAServer">http://opcfoundation.org/OPCUAServer</opc:Namespace>
	</opc:Namespaces>

	<opc:ObjectType SymbolicName="RequestType" BaseType="ua:BaseObjectType">
		<opc:Description>An object type representing a request to write data.</opc:Description>
		<opc:Children>
			<opc:Variable SymbolicName="REQ" DataType="ua:String" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite">
				<opc:Description>DIL writes ID (e.g. work oder number) into the node.</opc:Description>
			</opc:Variable>
			<opc:Variable SymbolicName="ACK" DataType="ua:String" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite">
				<opc:Description>SCADA writes ID into node to indicate acknowledgement.</opc:Description>
			</opc:Variable>
		</opc:Children>
	</opc:ObjectType>

	<opc:ObjectType SymbolicName="TriggerType" BaseType="ua:BaseObjectType">
		<opc:Description>An object type representing a trigger to read data.</opc:Description>
		<opc:Children>
			<opc:Variable SymbolicName="TRG" DataType="ua:UtcTime" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite">
				<opc:Description>Machine writes timestamp to indicate that values can be fetched/read.</opc:Description>
			</opc:Variable>
		</opc:Children>
	</opc:ObjectType>

	<opc:ObjectType SymbolicName="TriggerAckType" BaseType="ua:BaseObjectType">
		<opc:Description>An object type representing a TRIGGER to read data and an ACK to acknowledge reading the data.</opc:Description>
		<opc:Children>
			<opc:Variable SymbolicName="TRG" DataType="ua:UtcTime" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite">
				<opc:Description>Machine writes timestamp to indicate that values can be fetched/read.</opc:Description>
			</opc:Variable>
			<opc:Variable SymbolicName="ACK" DataType="ua:UtcTime" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite">
				<opc:Description>DIL writes timestamp to indicate that values can be overwritten.</opc:Description>
			</opc:Variable>
		</opc:Children>
	</opc:ObjectType>


	<!-- Generic Enumerations -->

	<opc:DataType SymbolicName="MachineStatusTypeEnumeration" BaseType="ua:Enumeration">
		<opc:Description>The status of a machine</opc:Description>
		<opc:Fields>
			<opc:Field Name="DOWN_Non_Scheduled" Identifier="1000000"> <opc:Description>The time when the equipment is not in a condition to perform its intended function due to unplanned downtime events (work order is assigned to machine).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_MAINTENANCE_DELAY" Identifier="1100000"> <opc:Description>The time during which the equipment cannot perform its intended function because it is waiting for either user or supplier personnel or parts associated with maintenance. The maintenance delay must be tracked separately from preventive maintenance time (see 2.400.000).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR" Identifier="1200000"> <opc:Description>The sum of the times for waiting for reaction, diagnosis, corrective action, equipment test and verification run, starting when the machine stops (failure is detected).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_WAITING_FOR_DIAGNOSIS" Identifier="1210000"> <opc:Description>The time from machine stop until start of diaonosis.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS" Identifier="1220000"> <opc:Description>Problem diagnosis including time for waiting for reaction</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS_MECHANICAL" Identifier="1220100"> <opc:Description>Mechanical problem (including hydraulic or pneumatic problems) as root cause of the failure, as far this can be detected automatically.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS_ELECTRICAL" Identifier="1220200"> <opc:Description>Electrical problem (including all hardware problems) as root cause of the failure, as far this can be detected automatically</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS_SOFTWARE" Identifier="1220300"> <opc:Description>Software problem (including configuration issues) as root cause of the failure, as far this can be detected automatically.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS_PROCESS" Identifier="1220400"> <opc:Description>Out-of-limits condition</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS_SUPPORT TOOLS" Identifier="1220500"> <opc:Description>Tool problem</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_DIAGNOSIS_HANDLING_EQUIPMENT" Identifier="1220600"> <opc:Description>Handling equipment problem</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_CORRECTIVE_ACTION" Identifier="1230000"> <opc:Description>The maintenance procedure employed to address an equipment failure and return the equipment to a condition where it can perform its intended function.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_EQUIPMENT_TEST" Identifier="1240000"> <opc:Description>The operation of the equipment to demonstrate equipment functionality.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_REPAIR_VERIFICATION_RUN" Identifier="1250000"> <opc:Description>The processing and evaluation of material (units) after corrective action to establish that the equipment is performing its intended function within specifications.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_CHANGE_OF_CONSUMABLES" Identifier="1300000"> <opc:Description>The time for the unscheduled interruption of operation to replenish operating supplies (consumables).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_OUT_OF_SPEC_INPUT" Identifier="1400000"> <opc:Description>The time when the equipment cannot perform its intended function solely as a result  of problems  created by out-of- specification or faulty inputs, including support tools (e.g. carrier I tray), material (unit), test data (e.g. metrology  tool  out  of  calibration), consumables.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_FACILITIES_RELATED" Identifier="1500000"> <opc:Description>The time when the equipment cannot perform its intended function solely as a result of out-of-specification facilities, including environmental (e.g. temperature, humidity, vibration, particle count), house hook-ups (e.g. power, cooling water, gases, etc.) and communication links (with other equipment or computers).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_FACILITIES_RELATED_ENVIRONMENTAL" Identifier="1510000"> <opc:Description>e.g. temperature, humidity, vibration, particle count</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_FACILITIES_RELATED_HOUSE_HOOKUPS" Identifier="1520000"> <opc:Description>e.g. power, cooling water, gases, exhaust, liquid nitrogen, etc.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_FACILITIES_RELATED_COMMUNICATION_LINKS" Identifier="1530000"> <opc:Description>Communication with other equipment or IT systems</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_SAFETY" Identifier="1600000"> <opc:Description>e.g. emergency stop</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_Non_Scheduled_QUALITY_ADJUSTMENT" Identifier="1700000"> <opc:Description>Unplanned machine downtime due to adjustments as corrective action for quality problems.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled" Identifier="2000000"> <opc:Description>The time when the equipment is not available to perform its intended function due to planned downtime events</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_MAINTENANCE_DELAY" Identifier="2100000"> <opc:Description>Compare to 1.100.000. The only difference is that the downtime has been scheduled (no work order).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_PRODUCTION_TEST" Identifier="2200000"> <opc:Description>The time for the planned interruption of equipment availability for evaluation of material (units), to confirm that the equipment is performing its intended function within specifications.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_CHANGE_OF_CONSUMABLE" Identifier="2300000"> <opc:Description>Compare to 1.300.000. The only difference is that the downtime has been scheduled (no work order).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_PREVENTIVE_MAINTENANCE" Identifier="2400000"> <opc:Description>The sum of the times for preventive action (predefined maintenance procedure at scheduled intervals to reduce likelihood of equipment failures or quality problems during operation), equipment test and verification run.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_PREVENTIVE_MAINTENANCE_PREVENTIVE_ACTION" Identifier="2410000"> <opc:Description>A predefined maintenance procedure at scheduled intervals, designed to reduce the likelihood of equipment failure during operation.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_PREVENTIVE_MAINTENANCE_EQUIPMENT_TEST" Identifier="2420000"> <opc:Description>The operation of the equipment to demonstrate equipment functionality.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_PREVENTIVE_MAINTENANCE_VERIFICATION_RUN" Identifier="2430000"> <opc:Description>The processing and evaluation of material (units) after preventive action to establish that the equipment is performing its intended function within specifications.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_FACILITIES_RELATED" Identifier="2500000"> <opc:Description>Compare to 1.500.000. The only difference is that the downtime has been scheduled (no work order).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_FACILITIES_RELATED_ENVIRONMENTAL" Identifier="2510000"> <opc:Description>Compare to 1.510.000. The only difference is that the downtime has been scheduled (no work order).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_FACILITIES_RELATED_HOUSE_HOOKUPS" Identifier="2520000"> <opc:Description>Compare to 1.520.000. The only difference is that the downtime has been scheduled (no work order).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_FACILITIES_RELATED_COMMUNICATION_LINKS" Identifier="2530000"> <opc:Description>Compare to 1.530.000. The only difference is that the downtime has been scheduled (no work order).</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SETUP" Identifier="2600000"> <opc:Description>The some of the time for waiting for setup, conversion, equipment test and verification run.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SETUP_WAITING_FOR_SETUP" Identifier="2610000"> <opc:Description>The time from selection of next work order until the conversion starts.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SETUP_CONVERSION" Identifier="2620000"> <opc:Description>The time required to complete an equipment alteration necessary to accommodate a change in process, material (unit), package configuration, etc.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SETUP_EQUIPMENT_TEST" Identifier="2630000"> <opc:Description>The operation of equipment to demonstrate equipment functionality.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SETUP_VERIFICATION_RUN" Identifier="2640000"> <opc:Description>The processing and evaluation of material (units) after conversion to establish that equipment isperforming its intended function within specifications.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_LINE_CLEARANCE" Identifier="2700000"> <opc:Description>Clearance of production line or machine when work order (batch / lot) is changed.</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SHIFT_CHANGE" Identifier="2800000"> <opc:Description>Machine downtime due to shift change activities</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SHIFT_CHANGE_SHIFT_TRANSMISSION" Identifier="2810000"> <opc:Description>Preparation for beginning, transmission or end of shift</opc:Description> </opc:Field>
			<opc:Field Name="DOWN_MaintenanceScheduled_SHIFT_CHANGE_STARTUP_SHUTDOWN" Identifier="2820000"> <opc:Description>Warming up, preparation or final cleaning of work centre (standard setup not included)</opc:Description> </opc:Field>
			<opc:Field Name="MAINTENANCE_NonScheduled" Identifier="3000000"> <opc:Description>The time when the equipment is not scheduled to be utilized in production (i.e. no work order is assigned to machine).</opc:Description> </opc:Field>
			<opc:Field Name="MAINTENANCE_NonScheduled_UNWORKED_SHIFTS" Identifier="3100000"> <opc:Description>e.g. weekends, holidays</opc:Description> </opc:Field>
			<opc:Field Name="MAINTENANCE_NonScheduled_OFFLINE_TRAINING" Identifier="3200000"> <opc:Description>Training which prevents the standard operation of the machine.</opc:Description> </opc:Field>
			<opc:Field Name="MAINTENANCE_NonScheduled_INSTALLATION_MODIFICATION" Identifier="3300000"> <opc:Description>e.g. installation, modification, rebuild, upgrade (hardware or software)</opc:Description> </opc:Field>
			<opc:Field Name="MAINTENANCE_NonScheduled_NO_ORDER_AVAILABLE" Identifier="3400000"> <opc:Description>Non-scheduled time because there is no work order available</opc:Description> </opc:Field>
			<opc:Field Name="ENGINEERING_Scheduled" Identifier="4000000"> <opc:Description>The time when the equipment is in a condition to perform its intended function, but is operated to conduct engineering experiments.</opc:Description> </opc:Field>
			<opc:Field Name="ENGINEERING_Scheduled_PROCESS_ENGINEERING" Identifier="4100000"> <opc:Description>e.g. process characterization</opc:Description> </opc:Field>
			<opc:Field Name="ENGINEERING_Scheduled_EQUIPMENT_ENGINEERING" Identifier="4200000"> <opc:Description>e.g. equipment evaluation</opc:Description> </opc:Field>
			<opc:Field Name="ENGINEERING_Scheduled_SOFTWARE_ENGINEERING" Identifier="4300000"> <opc:Description>e.g. software qualification</opc:Description> </opc:Field>
			<opc:Field Name="IDLE" Identifier="5000000"> <opc:Description>The time other than non-scheduled time, when the equipment is in a condition to perform its intended function, facilities are available, but it is not operated.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_OPERATOR_AVAILABLE" Identifier="5100000"> <opc:Description>Including irregular breaks, lunches, meetings, absence, operator needed at other work centre, etc.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_OPERATOR_AVAILABLE_BREAK" Identifier="5110000"> <opc:Description>Breaks, lunches</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_OPERATOR_AVAILABLE_MEETING" Identifier="5120000"> <opc:Description>Meeting, workshop</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_OPERATOR_AVAILABLE_ABSENCE" Identifier="5130000"> <opc:Description>e.g. sick</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_OPERATOR_AVAILABLE_NEEDED_AT_OTHER_WORK_CENTRE" Identifier="5140000"> <opc:Description>e.g. operator needs to support other work centre</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_MATERIAL_AVAILABLE" Identifier="5200000"> <opc:Description>In SEMI E10 this one is known as "NO UNITS AVAILABLE" (including no material due to lack of available support equipment, such as metrology tools).</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_SUPPORT_TOOLS_AVAILABLE" Identifier="5300000"> <opc:Description>e.g. cassettes, carriers, trays</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_INPUT_FROM_EXTERNAL_AUTOMATION_SYSTEMS" Identifier="5400000"> <opc:Description>Upstream problem, e.g. no material due to problem with previous machine or transport system. This is relevant only for connected machines. Please refer to 5.200.000 if the machine is not connected.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_OUTPUT_TO_EXTERNAL_AUTOMATION_SYSTEMS" Identifier="5500000"> <opc:Description>Downstream     problem,     e.g.     no machine  output  possible  due to  jam on    transport    system    behind    the machine.  This  is  relevant  only  for connected machines.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_AUXILIARY_MATERIAL_AVAILABLE" Identifier="5600000"> <opc:Description>Consumables, components or packing material not available</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_AUXILIARY_MATERIAL_AVAILABLE_NO_CONSUMABLES_AVAILABLE" Identifier="5610000"> <opc:Description>Consumables have not been replenished in time.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_AUXILIARY_MATERIAL_AVAILABLE_NO_COMPONENTS_AVAILABLE" Identifier="5620000"> <opc:Description>Components have not been replenished in time.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_NO_AUXILIARY_MATERIAL_AVAILABLE_NO_PACKING_MATERIAL_AVAILABLE" Identifier="5630000"> <opc:Description>Packing material has not been replenished in time.</opc:Description> </opc:Field>
			<opc:Field Name="IDLE_MISSING_DOCUMENTS" Identifier="5700000"> <opc:Description>Required documents or information have not been provided in time.</opc:Description> </opc:Field>
			<opc:Field Name="UP" Identifier="9000000"> <opc:Description>Equipment is performing its intended function</opc:Description> </opc:Field>
			<opc:Field Name="UP_REGULAR PRODUCTION" Identifier="9100000"> <opc:Description>Including loading and unloading of material (units)</opc:Description> </opc:Field>
			<opc:Field Name="UP_WORK FOR THIRD PARTIES" Identifier="9200000"> <opc:Description></opc:Description> </opc:Field>
			<opc:Field Name="UP_REWORK" Identifier="9300000"> <opc:Description></opc:Description> </opc:Field>
			<opc:Field Name="UP_ENGINEERING RUNS" Identifier="9400000"> <opc:Description>e.g. new applications</opc:Description> </opc:Field>
		</opc:Fields>
	</opc:DataType>

	<opc:ObjectType SymbolicName="GenericSensorType" BaseType="ua:BaseObjectType">
		<opc:Description>A generic sensor.</opc:Description>
		<opc:Children>
			<opc:Object SymbolicName="TriggerAck" TypeDefinition="TriggerAckType" SupportsEvents="true">
				<opc:BrowseName>TriggerAck</opc:BrowseName>
			</opc:Object>
			<opc:Variable SymbolicName="Value" DataType="ua:Double" ValueRank="Scalar" TypeDefinition="ua:AnalogItemType" AccessLevel="Read" />
		</opc:Children>
	</opc:ObjectType>

	<opc:ObjectType SymbolicName="TemperatureSensor" BaseType="GenericSensorType">
		<opc:Description>A temperature sensor.</opc:Description>
	</opc:ObjectType>

	<opc:ObjectType SymbolicName="ProximitySensor" BaseType="ua:BaseObjectType">
		<opc:Description>A proximity sensor.</opc:Description>
		<opc:Children>
			<opc:Object SymbolicName="Trigger" TypeDefinition="TriggerType" SupportsEvents="true">
				<opc:BrowseName>Trigger</opc:BrowseName>
			</opc:Object>
			<opc:Variable SymbolicName="Value" DataType="ua:Double" ValueRank="Scalar" TypeDefinition="ua:AnalogItemType" AccessLevel="Read" />
		</opc:Children>
	</opc:ObjectType>

	<opc:Method SymbolicName="StartMethodType">
		<opc:Description>A method used to start the motor.</opc:Description>
		<opc:InputArguments>
			<opc:Argument Name="Duration" DataType="ua:Int32" IsOptional="true">
				<opc:Description>Amount of time the motor shall run.</opc:Description>
			</opc:Argument>
			<opc:Argument Name="Turbo" DataType="ua:Boolean" IsOptional="true">
				<opc:Description>Whether to run in turbo mode.</opc:Description>
			</opc:Argument>
		</opc:InputArguments>
	</opc:Method>

	<opc:ObjectType SymbolicName="MotorConveyor" BaseType="ua:BaseObjectType">
		<opc:Description>A motor.</opc:Description>
		<opc:Children>
			<opc:Variable SymbolicName="Name" DataType="ua:String" ValueRank="Scalar" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite" />
			<opc:Variable SymbolicName="Uptime" DataType="ua:UtcTime" ValueRank="Scalar" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite" />
			<opc:Variable SymbolicName="Speed" DataType="ua:Double" ValueRank="Scalar" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite" />
			<opc:Variable SymbolicName="Heat" DataType="ua:Number" ValueRank="Scalar" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite" />
			<opc:Variable SymbolicName="Status" DataType="MachineStatusTypeEnumeration" TypeDefinition="ua:AnalogItemType" AccessLevel="ReadWrite">
				<opc:Description>Status of motor based on SEMI E-10</opc:Description>
			</opc:Variable>
			<opc:Method SymbolicName="Start" TypeDefinition="StartMethodType" ModellingRule="Mandatory">
				<opc:Description>A method used to start the motor.</opc:Description>
			</opc:Method>
			<opc:Method SymbolicName="Stop" ModellingRule="Mandatory">
				<opc:Description>A method used to stop the motor.</opc:Description>
			</opc:Method>
		</opc:Children>
	</opc:ObjectType>

	<opc:ObjectType SymbolicName="MachineType" BaseType="ua:FolderType">
		<opc:Children>
			<opc:Object SymbolicName="Temperature" TypeDefinition="TemperatureSensor" SupportsEvents="true">
				<opc:BrowseName>Temperature</opc:BrowseName>
			</opc:Object>
			<opc:Object SymbolicName="Proximity" TypeDefinition="ProximitySensor" SupportsEvents="true">
				<opc:BrowseName>Proximity</opc:BrowseName>
			</opc:Object>
			<opc:Object SymbolicName="Conveyor" TypeDefinition="MotorConveyor" SupportsEvents="true">
				<opc:BrowseName>Conveyor</opc:BrowseName>
			</opc:Object>
		</opc:Children>
	</opc:ObjectType>
	
	<opc:ObjectType SymbolicName="OPCUAServerType" BaseType="ua:BaseObjectType" SupportsEvents="true">
		<opc:Description>A production batch plant.</opc:Description>
		<opc:Children>
			<opc:Object SymbolicName="Machine" TypeDefinition="MachineType" SupportsEvents="true">
				<opc:BrowseName>Machine</opc:BrowseName>
			</opc:Object>
		</opc:Children>
	</opc:ObjectType>
		
	<opc:Object SymbolicName="OPCUAServer1" TypeDefinition="OPCUAServerType" SupportsEvents="true">
		<opc:BrowseName>Line</opc:BrowseName>
		<opc:References>
			<opc:Reference IsInverse="true">
				<opc:ReferenceType>ua:Organizes</opc:ReferenceType>
				<opc:TargetId>ua:ObjectsFolder</opc:TargetId>
			</opc:Reference>
		</opc:References>
	</opc:Object>
	
</opc:ModelDesign>
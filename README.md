# OPC UA Simulation Server

The OPC UA Simulation Server is a milo server that has some additional features:
 -  Event mirroring: all method calls are mirrored as events and can be captured and validated.
 -  Trigger event: A method can be used to trigger an arbitrary event.
 -  All nodes are writable.
 -  Arbitrary NodeSet2 XML files can be used with this server.

## Use the server

Use the following environment variables to configure the server:
 - BIND_ADDRESS: The bind address. Default: 0.0.0.0
 - EXTERNAL_ADDRESS: The external address the server will be accessed via. Default: 0.0.0.0
 - BIND_PORT_TCP: The TCP bind port for the OPC UA server. Default: 4840
 - BIND_PORT_HTTPS: The HTTPS bind port for the OPC UA server. Default: disabled
 - AUTH_USERNAME: The username to use for authentication. If it contains 'anon', no user will be used for authentication. Default: admin
 - AUTH_PASSWORD: The password to use for authentication. Default: admin
 - NODESET2: The path to the nodeset2 file. Default: A bundled example nodeset2 file is used.

### Run the server using the jar file

Download the jar file from the [package registry](https://gitlab.com/kernblick_oss/opc-ua-simulator/opc-ua-server/-/packages) and run it as follows:

```bash
java -jar opcua-server-x.x.x.jar -help
java -jar opcua-server-x.x.x.jar
```

### Run the server using docker

Simply execute the following commands to run the docker container:

```bash
docker run -it registry.gitlab.com/kernblick_oss/opc-ua-simulator/opc-ua-server -help
docker run -it registry.gitlab.com/kernblick_oss/opc-ua-simulator/opc-ua-server
```

## Use with maven

Copy and paste this inside your pom.xml dependencies block.

```xml
<dependency>
  <groupId>de.kernblick.opcuasimulator</groupId>
  <artifactId>opcua-server</artifactId>
  <version>1.1.0</version>
</dependency>
```

If you haven't already done so, you will need to add the below to your pom.xml file.

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/52528962/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/52528962/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/52528962/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

## Development: Generate UANodeSet Java Sources

```bash
xjc -encoding "UTF-8" "src/main/resources/UANodeSet.xsd" -d "src/main/java/com/digitalpetri/opcua/nodeset/generated"
```
